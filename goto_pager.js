// jQuery safe anonymous function
;(function($){
	Drupal.behaviors.goto_pager = function(context)
	{
		if (Drupal.settings && Drupal.settings.views && Drupal.settings.views.ajaxViews) {
			var ajax_path = Drupal.settings.views.ajax_path;

		    // If there are multiple views this might've ended up showing up multiple times.
		    if (ajax_path.constructor.toString().indexOf("Array") != -1)
			{
		      ajax_path = ajax_path[0];
		    }
		
			// add "pager-ajax" class to each ÅJAX view pager
			$.each(Drupal.settings.views.ajaxViews, function(i, settings){
				var view = '.view-dom-id-' + settings.view_dom_id;

				if (!$(view).size())
				{
			        // Backward compatibility: if 'views-view.tpl.php' is old and doesn't
			        // contain the 'view-dom-id-#' class, we fall back to the old way of
			        // locating the view:
			        view = '.view-id-' + settings.view_name + '.view-display-id-' + settings.view_display_id;
		      	}

				$(view).data('settings', settings).find('.pager').addClass('pager-ajax');
			});

			// add pager to AJAX Views pager
			$('.pager-goto').filter('.pager-ajax').bind('change', function(e){
				var $this = $(this);
				var anchor = $('.pager-previous a, .pager-next a', $this).filter(':first');
				var url = anchor.url();

				// get pager parameters
				var target = $(e.target);
				var current = parseInt($this.attr('data-current'));
				var total = parseInt($this.attr('data-total'));

				// only continue if target is goto text input and page number is valid
				if (target.is('input[type=text][name^=goto]') && (val = parseInt(target.val())) && !isNaN(val) && (val != current + 1) && (val <= total) && (val > 0))
				{
					var target = $(this).parents('.view').get(0);
					var settings = $(target).data('settings');
					var viewData = { 'js': 1 };

					$.extend(viewData, settings, {page: val - 1});

					$.ajax({
						url: ajax_path,
						type: 'GET',
						data: viewData,
						dataType: 'json',
						success: function(response){
							// Scroll to the top of the view. This will allow users
		                    // to browse newly loaded content after e.g. clicking a pager
		                    // link.
							var offset = $(target).offset();

							// We can't guarantee that the scrollable object should be
							// the body, as the view could be embedded in something
							// more complex such as a modal popup. Recurse up the DOM
							// and scroll the first element that has a non-zero top.
							var scrollTarget = target;

							while ($(scrollTarget).scrollTop() == 0 && $(scrollTarget).parent())
							{
								scrollTarget = $(scrollTarget).parent()
							}

							// Only scroll upward
							if (offset.top - 10 < $(scrollTarget).scrollTop())
							{
								$(scrollTarget).animate({scrollTop: (offset.top - 10)}, 500);
							}

							// Call all callbacks.
							if (response.__callbacks)
							{
								$.each(response.__callbacks, function(i, callback) {
							    	eval(callback)(target, response);
								});
							}
						},
						error: function(xhr){
							Drupal.Views.Ajax.handleErrors(xhr, ajax_path);
						}
					});
				}
			})
		}

		// add change event to AJAX pager
		$('.pager-goto').not('.pager-ajax').each(function(i){
			var $this = $(this);
			var url = $(document).url();

			// bind events to pager
			$this.bind('click', function(e){
				// get pager parameters
				var target = $(e.target);
				var current = parseInt($this.attr('data-current'));
				var total = parseInt($this.attr('data-total'));

				// check if anchor is previous anchor
				if (target.is('a') && target.parent('.pager-previous').length && current > 0)
				{
					url.param('page', current - 1);
				}

				if (target.is('a') && target.parent('.pager-next').length && (current < total - 1))
				{
					url.param('page', current + 1);
				}

				e.preventDefault();
			}).bind('change', function(e){
				// get pager parameters
				var target = $(e.target);
				var current = parseInt($this.attr('data-current'));
				var total = parseInt($this.attr('data-total'));

				// only continue if target is goto text input and page number is valid
				if (target.is('input[type=text][name^=goto]') && (val = parseInt(target.val())) && !isNaN(val) && (val != current + 1) && (val <= total) && (val > 0))
				{
					url.param('page', val - 1);
				}
			});
		});
	}
})(jQuery);